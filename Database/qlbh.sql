/*
Navicat MySQL Data Transfer

Source Server         : MySQL_Nam
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : qlbh

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-08-23 08:01:02
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `bills`
-- ----------------------------
DROP TABLE IF EXISTS `bills`;
CREATE TABLE `bills` (
  `DocEntry` int(11) NOT NULL AUTO_INCREMENT,
  `DocType` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `U_MoCash` decimal(10,0) DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `CreateBy` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DocEntrySO` int(11) DEFAULT NULL,
  PRIMARY KEY (`DocEntry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of bills
-- ----------------------------

-- ----------------------------
-- Table structure for `customers`
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `CustomerID` int(11) NOT NULL,
  `Fullname` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Region` int(11) DEFAULT NULL,
  `Address` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Taxcode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Fax` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mobile` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Website` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Taikhoan` int(11) DEFAULT NULL,
  `Bank` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LimitOwe` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CurrOwe` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Discount` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Yahoo` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Skype` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Contact` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `UDesc` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`CustomerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of customers
-- ----------------------------

-- ----------------------------
-- Table structure for `dealdetails`
-- ----------------------------
DROP TABLE IF EXISTS `dealdetails`;
CREATE TABLE `dealdetails` (
  `ID` int(11) NOT NULL,
  `DocEntry` int(11) DEFAULT NULL,
  `ItemCode` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `U_Price` decimal(10,0) DEFAULT NULL,
  `WhsCode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `U_TMoney` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of dealdetails
-- ----------------------------

-- ----------------------------
-- Table structure for `dealheader`
-- ----------------------------
DROP TABLE IF EXISTS `dealheader`;
CREATE TABLE `dealheader` (
  `DocEntry` int(11) NOT NULL,
  `DocType` int(11) DEFAULT NULL,
  `DocStatus` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DistCode` int(11) DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  `Note` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `CreateBy` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `UpdateDate` datetime DEFAULT NULL,
  `UpdateBy` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`DocEntry`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of dealheader
-- ----------------------------

-- ----------------------------
-- Table structure for `departments`
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `DepartID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`DepartID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of departments
-- ----------------------------

-- ----------------------------
-- Table structure for `distributors`
-- ----------------------------
DROP TABLE IF EXISTS `distributors`;
CREATE TABLE `distributors` (
  `DistCode` int(11) NOT NULL,
  `DistName` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Region` int(11) DEFAULT NULL,
  `Address` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Taxcode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Fax` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mobile` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Website` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Taikhoan` int(11) DEFAULT NULL,
  `Bank` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LimitOwe` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CurrOwe` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Discount` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Jobtitle` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`DistCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of distributors
-- ----------------------------

-- ----------------------------
-- Table structure for `employees`
-- ----------------------------
DROP TABLE IF EXISTS `employees`;
CREATE TABLE `employees` (
  `EmployeeCode` int(11) NOT NULL,
  `EmployeeName` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Jobtitle` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mobile` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Manager` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DepartmentID` int(11) DEFAULT NULL,
  `StateSelf` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`EmployeeCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of employees
-- ----------------------------

-- ----------------------------
-- Table structure for `goodsgroup`
-- ----------------------------
DROP TABLE IF EXISTS `goodsgroup`;
CREATE TABLE `goodsgroup` (
  `GGID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`GGID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of goodsgroup
-- ----------------------------

-- ----------------------------
-- Table structure for `inventorys`
-- ----------------------------
DROP TABLE IF EXISTS `inventorys`;
CREATE TABLE `inventorys` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ItemCode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WhsCode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvMin` int(11) DEFAULT NULL,
  `OnHand` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of inventorys
-- ----------------------------

-- ----------------------------
-- Table structure for `items`
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `ItemCode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ItemName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `GrpGoodsId` int(11) DEFAULT NULL,
  `Unit` int(11) DEFAULT NULL,
  `LineID_NSX` int(11) DEFAULT NULL,
  `WhsCode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MadeIn` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PerTax` int(11) DEFAULT NULL,
  `TypeItem` int(11) DEFAULT NULL,
  `TypeName` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DistCode` int(11) DEFAULT NULL,
  `PriceBuy` decimal(10,0) DEFAULT NULL,
  `PriceSale` decimal(10,0) DEFAULT NULL,
  `PriceTail` decimal(10,0) DEFAULT NULL,
  `U_DateUse` datetime DEFAULT NULL,
  `U_Desc` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ItemCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of items
-- ----------------------------

-- ----------------------------
-- Table structure for `logsystem`
-- ----------------------------
DROP TABLE IF EXISTS `logsystem`;
CREATE TABLE `logsystem` (
  `LogID` int(11) NOT NULL AUTO_INCREMENT,
  `S_User` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PC` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TimeAction` datetime DEFAULT NULL,
  `Function` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Action` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ObjType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`LogID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of logsystem
-- ----------------------------

-- ----------------------------
-- Table structure for `regions`
-- ----------------------------
DROP TABLE IF EXISTS `regions`;
CREATE TABLE `regions` (
  `RegionID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`RegionID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of regions
-- ----------------------------
INSERT INTO `regions` VALUES ('1', 'Khu vực 1', null, 'A');
INSERT INTO `regions` VALUES ('2', 'Khu vực 2', null, 'A');
INSERT INTO `regions` VALUES ('3', 'Khu vực 3', null, 'A');
INSERT INTO `regions` VALUES ('4', 'Khu vực 4', null, 'A');
INSERT INTO `regions` VALUES ('5', 'Khu vực 5', null, 'A');
INSERT INTO `regions` VALUES ('6', 'Khu vực 6', null, 'A');
INSERT INTO `regions` VALUES ('7', 'Khu vực 7', null, 'A');
INSERT INTO `regions` VALUES ('8', 'Khu vực 8', null, 'A');
INSERT INTO `regions` VALUES ('9', 'Khu vực 9', null, 'A');
INSERT INTO `regions` VALUES ('10', 'Khu vực 10', null, 'A');

-- ----------------------------
-- Table structure for `units`
-- ----------------------------
DROP TABLE IF EXISTS `units`;
CREATE TABLE `units` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of units
-- ----------------------------
INSERT INTO `units` VALUES ('1', 'cái', 'cái', 'A');
INSERT INTO `units` VALUES ('2', 'mét', 'mét', 'A');
INSERT INTO `units` VALUES ('3', 'hộp', null, 'A');
INSERT INTO `units` VALUES ('4', 'chai', null, 'A');
INSERT INTO `units` VALUES ('5', 'gam', null, 'A');

-- ----------------------------
-- Table structure for `useraccount`
-- ----------------------------
DROP TABLE IF EXISTS `useraccount`;
CREATE TABLE `useraccount` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Password` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CreateDate` datetime DEFAULT NULL,
  `Createby` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Name` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Role` int(11) DEFAULT NULL,
  `U_Desc` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of useraccount
-- ----------------------------

-- ----------------------------
-- Table structure for `warehouses`
-- ----------------------------
DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE `warehouses` (
  `WhsCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `WhsName` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Manager` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Email` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Fax` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Contact` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Note` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`WhsCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of warehouses
-- ----------------------------
INSERT INTO `warehouses` VALUES ('33001', 'Kho hàng cũ', null, null, null, null, null, null, null, null, null);
INSERT INTO `warehouses` VALUES ('33010', 'Kho điện-điện tử', null, null, null, null, null, null, null, null, null);
INSERT INTO `warehouses` VALUES ('33020', 'Kho gia dụng', null, null, null, null, null, null, null, null, null);
INSERT INTO `warehouses` VALUES ('33030', 'Kho điện lạnh', null, null, null, null, null, null, null, null, null);
