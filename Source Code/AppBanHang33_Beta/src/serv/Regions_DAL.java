/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.*;
/**
 *
 * @author VanNTV
 */
public class Regions_DAL {
    private static final Session se =  HibernateUtil.getSession();
    
    public static List<Regions> GetAllRegions()
    {
        List<Regions> list = new ArrayList<Regions>();
        try
        {
            Query query = se.createQuery("from Regions");
            list = query.list();
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return list;
    }
    
    public static String GetRegionNameById(int id)
    {
        Regions region = new Regions();
        try
        {
            Query query = se.createQuery("select r from Regions as r where RegionId = :id");
            query.setParameter("id", id);
            List<Regions> l = query.list();         
            region = l.get(0);
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return region.getName();
    }
    
    public static Regions GetRegionById(int id)
    {
        Regions region = new Regions();
        try
        {
            Query query = se.createQuery("select r from Regions as r where RegionID = :id");
            query.setParameter("id", id);
            Object r = query.list().get(0);
            if(r != null){
                region = (Regions)r;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return region;
    }

    public static boolean InsertNewRegion(Regions region)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.save(region);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean UpdateRegion(Regions region)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.merge(region);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean DeleteRegion(Regions region)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            Regions object = (Regions) se.get(Regions.class, region.getRegionId());
            se.delete(object);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
}
