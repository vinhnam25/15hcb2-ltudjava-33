/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Useraccount;

/**
 *
 * @author tvnam
 */
public class Useraccount_DAL {
    private static final Session se =  HibernateUtil.getSession();
    
        public static List<Object[]> SignIn(String username, String password)
    {
        List<Object[]> obj = null;        
        try
        {
            String hql = "from Useraccount s, Employees e where s.eplCode = e.employeeCode and s.username = :username and s.password = :password";
            //System.out.println(hql);
            Query q = se.createQuery(hql);
            q.setParameter("username", username);
            q.setParameter("password", password);
            obj = q.list();         

        }
        catch(Exception ex)
        {
            ex.toString();
        }
        
        return obj;
    } 

    public static boolean UpdatePassword(String username, String newPassword)
    {
        String query = "UPDATE Useraccount SET password = '"+ newPassword +"' WHERE username = '" + username + "'";
        try {
            se.getTransaction().begin();
            se.createSQLQuery(query).executeUpdate();
            se.getTransaction().commit();
            return true;
        }
        catch (HibernateException erro){
            se.getTransaction().rollback();
        }
        
        return false;
    }
}
