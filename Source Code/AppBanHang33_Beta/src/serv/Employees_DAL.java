/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Employees;
/**
 *
 * @author dunglogicode
 */
public class Employees_DAL {
    private static final Session se =  HibernateUtil.getSession();
    
    public static List<Employees> GetAllEmployees()
    {
        Query query = se.createQuery("from Employees");
        List<Employees> list = query.list();
        return list;
    }
    
    public static int GetMaxEmployeeID()
    {
        Query query = se.createQuery("select MAX(u.id) from Employees as u");
        int max = (Integer)query.uniqueResult();
        return max;
    }
    
    public static Employees GetEmployeeById(int id)
    {
        Employees e = new Employees();
        try
        {
            Query query = se.createQuery("select e from Employees as e where EmployeeCode = :id");
            query.setParameter("id", id);
            Object w = query.list().get(0);
            if(w != null){
                e = (Employees)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return e;
    }
    
    public static Employees GetEmployeeByName(String name)
    {
        Employees e = new Employees();
        try
        {
            Query query = se.createQuery("select e from Employees as e where employeename = :name");
            query.setParameter("name", name);
            Object w = query.list().get(0);
            if(w != null){
                e = (Employees)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return e;
    }
    
    public static boolean InsertNewEmployee(Employees em)
    {
        Transaction trans = se.beginTransaction();
        try
        {   
            se.save(em);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean UpdateEmployee(Employees em)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.merge(em);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean DeleteEmployee(Employees em)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            Employees ob = (Employees ) se.get(Employees.class, em.getEmployeeCode());
            se.delete(ob);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static List<Employees> SearchForEmployees(String ma, String ten, String cv, int bp, int ql)
    {
        String hql = "from Employees where (employeecode = :ma or :ma = 0)  and (employeename = :ten or :ten='') and (jobtitle = :cv or :cv ='') and (departmentid = :bp or :bp=0) or (manager = :ql or :ql=0)";
        //String hql = "Employees where id = :ma or name = :ten or jobtitle = :cv or departmentid = :bp or manager = :ql";
        System.out.println(hql);
        Query query = se.createQuery(hql);
        query.setParameter("ma", ma);
        query.setParameter("ten", ten);
        query.setParameter("cv", cv);
        query.setParameter("bp", bp);
        query.setParameter("ql", ql); 
        List<Employees> list = query.list();
        return list;
    }
}
