/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Warehouses;

/**
 *
 * @author NgocLam
 */
public class Warehouses_DAL {
    private static final Session se =  HibernateUtil.getSession();
    
    public static List<Warehouses> GetAllWarehouses()
    {
        List<Warehouses> list = new ArrayList<Warehouses>();
        try
        {
            Query query = se.createQuery("from Warehouses");
            list = query.list();
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return list;
    }
    
    public static Warehouses GetWarehouseById(String id)
    {
        Warehouses warehouse = new Warehouses();
        try
        {
            Query query = se.createQuery("select w from Warehouses as w where WhsCode = :id");
            query.setParameter("id", id);
            Object w = query.list().get(0);
            if(w != null){
                warehouse = (Warehouses)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return warehouse;
    }
    
    public static String GetWarehouseNameById(int id)
    {
        Warehouses warehouse = new Warehouses();
        try
        {
            String hql = "from Warehouses w where w.WhsCode = :id";
            Query query = se.createQuery(hql);
            query.setParameter("id", id);
            List<Warehouses> l = query.list();         
            warehouse = l.get(0);
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return warehouse.getWhsName();
    }
    
    public static Warehouses GetWarehouseByName(String name)
    {
        Warehouses warehouse = new Warehouses();
        try
        {
            String hql = "from Warehouses w where w.WhsName = :name";
            Query query = se.createQuery(hql);
            query.setParameter("name", name);
            List<Warehouses> l = query.list();         
            warehouse = l.get(0);
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return warehouse;
    }
    
    public static boolean InsertNewWarehouse(Warehouses warehouse)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.save(warehouse);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean UpdateWarehouse(Warehouses warehouse)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.merge(warehouse);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean DeleteWarehouse(Warehouses warehouse)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            Warehouses object = (Warehouses) se.get(Warehouses.class, warehouse.getWhsCode());
            se.delete(object);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
}
