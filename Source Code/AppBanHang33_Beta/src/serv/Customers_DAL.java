/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Customers;

/**
 *
 * @author NgocLam
 */
public class Customers_DAL {
    private static final Session se =  HibernateUtil.getSession();
        
    public static List<Customers> GetAllCustomers()
    {
        List<Customers> list = new ArrayList<Customers>();
        try
        {
            Query query = se.createQuery("from Customers");
            list = query.list();
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return list;
    }
    
    public static Customers GetCustomerById(Integer id)
    {
        Customers customer = new Customers();
        try
        {
            Query query = se.createQuery("select w from Customers as w where CustomerID = :id");
            query.setParameter("id", id);
            Object w = query.list().get(0);
            if(w != null){
                customer = (Customers)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return customer;
    }
    
    public static boolean InsertNewCustomer(Customers customer)
    {
        Transaction trans = se.beginTransaction();
        try
        {
            se.save(customer);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            trans.commit();
            ex.toString();
        }
        return false;
    }
    
    public static boolean UpdateCustomer(Customers customer)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.merge(customer);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean DeleteCustomer(Customers customer)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            Customers object = new Customers();
            object = (Customers) se.get(Customers.class, customer.getCustomerId());
            se.delete(object);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static int GetKeyInsert()
    {
        int kq = 0;
        try
        {
            String hql = "select MAX(s.customerId) from Customers s";
            int mk = (int)se.createQuery(hql).uniqueResult();
            mk++;
            kq = mk;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return kq;
    }
}
