/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Goodsgroup;

/**
 *
 * @author dunglogicode
 */
public class Goodsgroup_DAL {
    private static final Session se =  HibernateUtil.getSession();
    public static List<Goodsgroup> GetAllGoodsgroups()
    {
        Query query = se.createQuery("from Goodsgroup");
        List<Goodsgroup> list = query.list();
        return list;
    }
    public static int GetMaxGGID()
    {
        Query query = se.createQuery("select MAX(g.id) from Goodsgroup as g");
        int max = (Integer)query.uniqueResult();
        return max;
    }
    public static Goodsgroup GetGoodsgroupByID(int id)
    {
        Query query = se.createQuery("select g from Goodsgroup as g where id = :" + id);
        Object de = query.getFirstResult();
        return (Goodsgroup)de;
    }
    public static Goodsgroup GetGoodsgroupByName(String name)
    {
        Query query = se.createQuery("select g from Goodsgroup as g where name = :name" );
        query.setParameter("name", name);
        Object de = query.getFirstResult();
        return (Goodsgroup)de;
    }
    public static boolean InsertNewGoodsgroup(Goodsgroup gg)
    {
        
        Transaction transins = se.beginTransaction();
        try
        {
            
            se.save(gg);
            transins.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean UpdateGoodsgroup(Goodsgroup gg)
    {
        try
        {
            Transaction transupd = se.beginTransaction();
            se.merge(gg);
            transupd.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean DeleteGoodsgroup(Goodsgroup gg)
    {
        try
        {
            Transaction transdel = se.beginTransaction();
            Goodsgroup ob = (Goodsgroup ) se.get(Goodsgroup.class, gg.getGgid());
            se.delete(ob);
            transdel.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
}
