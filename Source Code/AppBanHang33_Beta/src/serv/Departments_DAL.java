/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Departments;

/**
 *
 * @author dunglogicode
 */
public class Departments_DAL {
    private static final Session se =  HibernateUtil.getSession();
    public static List<Departments> GetAllDepartments()
    {
        Query query = se.createQuery("from Departments");
        List<Departments> list = query.list();
        return list;
    }
    public static int GetMaxDepartID()
    {
        int max = 0;
        Query query = se.createQuery("select MAX(d.id) from Departments as d");
        if(query.uniqueResult() == null) max = 0;
        else max = (Integer)query.uniqueResult();
        return max;
    }
    public static Departments GetDepartmentById(int id)
    {
        Departments de = new Departments();
        try
        {
            Query query = se.createQuery("select d from Departments as d where DepartId = :id");
            query.setParameter("id", id);
            Object w = query.list().get(0);
            if(w != null){
                de = (Departments)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return de;
    }
    public static Departments GetDepartmentByName(String name)
    {
        Departments de = new Departments();
        try
        {
            Query query = se.createQuery("select d from Departments as d where name = :name");
            query.setParameter("name", name);
            Object w = query.list().get(0);
            if(w != null){
                de = (Departments)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return de;
    }
    public static boolean InsertNewDepartment(Departments de)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.save(de);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean UpdateDepartment(Departments de)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.merge(de);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean DeleteDepartment(Departments de)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            Departments ob = (Departments ) se.get(Departments.class, de.getDepartId());
            se.delete(ob);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
}
