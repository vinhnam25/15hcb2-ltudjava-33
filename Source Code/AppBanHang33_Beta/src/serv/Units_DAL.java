/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Units;

/**
 *
 * @author dunglogicode
 */
public class Units_DAL {
    private static final Session se =  HibernateUtil.getSession();
    
    public static List<Units> GetAllUnits()
    {
        Query query = se.createQuery("from Units");
        List<Units> list = query.list();
        return list;
    }
    
    public static int GetMaxUnitID()
    {
        Query query = se.createQuery("select MAX(u.id) from Units as u");
        int max = (Integer)query.uniqueResult();
        return max;
    }
    
    public static Units GetUnitByID(int id)
    {
        Query query = se.createQuery("select u from Units as u where id = :id");
        query.setParameter("id", id);
        Object u = query.getFirstResult();
        return (Units)u;
    }
    
    public static Units GetUnitByName(String name)
    {
        Query query = se.createQuery("select u from Units as u where name = :name");
        query.setParameter("name", name);
        Object u = query.getFirstResult();
        return (Units)u;
    }
    
    public static boolean InsertNewUnit(Units unit)
    {
        Transaction trans = se.beginTransaction();
        try
        {   
            se.save(unit);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean UpdateUnit(Units unit)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.merge(unit);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean DeleteUnit(Units unit)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            Units ob = (Units ) se.get(Units.class, unit.getId());
            se.delete(ob);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
}
