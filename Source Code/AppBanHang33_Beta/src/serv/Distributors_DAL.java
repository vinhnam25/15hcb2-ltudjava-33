/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Distributors;

/**
 *
 * @author NgocLam
 */
public class Distributors_DAL {
    private static final Session se =  HibernateUtil.getSession();
    
    public static List<Distributors> GetAllDistributors()
    {
        List<Distributors> list = new ArrayList<Distributors>();
        try
        {
            Query query = se.createQuery("from Distributors");
            list = query.list();
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return list;
    }
    
    public static String GetDistributorNameById(int id)
    {
        Distributors distributor = new Distributors();
        try
        {
            String hql = "from Distributors d where d.DistCode = :id";
            Query query = se.createQuery(hql);
            query.setParameter("id", id);
            List<Distributors> l = query.list();         
            distributor = l.get(0);
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return distributor.getDistName();
    }
    
    public static Distributors GetDistributorById(int id)
    {
        Distributors distributor = new Distributors();
        try
        {
            Query query = se.createQuery("select w from Distributors as w where DistCode = :id");
            query.setParameter("id", id);
            Object w = query.list().get(0);
            if(w != null){
                distributor = (Distributors)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return distributor;
    }
    
    public static Distributors GetDistributorByName(String name)
    {
        Distributors distributor = new Distributors();
        try
        {
            Query query = se.createQuery("select w from Distributors as w where DistName = :name");
            query.setParameter("name", name);
            Object w = query.list().get(0);
            if(w != null){
                distributor = (Distributors)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return distributor;
    }
    
    public static boolean InsertNewDistributor(Distributors distributor)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.save(distributor);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean UpdateDistributor(Distributors distributor)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.merge(distributor);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean DeleteDistributor(Distributors distributor)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            Distributors object = (Distributors) se.get(Distributors.class, distributor.getDistCode());
            se.delete(object);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static int GetKeyInsert()
    {
        int kq = 0;
        try
        {
            String hql = "select MAX(s.distCode) from Distributors s";
            int mk = (int)se.createQuery(hql).uniqueResult();
            mk++;
            kq = mk;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return kq;
    }
}
