/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;
import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Items;
/**
 *
 * @author dunglogicode
 */
public class Items_DAL {
    private static final Session se =  HibernateUtil.getSession();
    
    public static List<Items> GetAllItems()
    {
        Query query = se.createQuery("from Items");
        List<Items> list = query.list();
        return list;
    }
    
    public static int GetMaxItemID()
    {
        Query query = se.createQuery("select MAX(i.id) from Items as i");
        int max = (Integer)query.uniqueResult();
        return max;
    }
    
    public static Items GetItemById(int id)
    {
        Items i = new Items();
        try
        {
            Query query = se.createQuery("select i from Items as i where ItemCode = :id");
            query.setParameter("id", id);
            Object w = query.list().get(0);
            if(w != null){
                i = (Items)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return i;
    }
    
    public static Items GetItemByName(String name)
    {
        Items i = new Items();
        try
        {
            Query query = se.createQuery("select i from Items as i where itemname = :name ");
            query.setParameter("name", name);
            Object w = query.list().get(0);
            if(w != null){
                i = (Items)w;
            }
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return i;
    }
    
    public static boolean InsertNewItem(Items i)
    {
        Transaction trans = se.beginTransaction();
        try
        {   
            se.save(i);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean UpdateItem(Items i)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            se.merge(i);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static boolean DeleteItem(Items i)
    {
        try
        {
            Transaction trans = se.beginTransaction();
            Items ob = (Items ) se.get(Items.class, i.getItemCode());
            se.delete(ob);
            trans.commit();
            return true;
        }
        catch(Exception ex)
        {
            ex.toString();
        }
        return false;
    }
    
    public static List<Items> SearchForItems(String ma, String ten, int gg, int dv, String xx)
    {
        String hql = "from Items where (Itemcode = :ma or :ma = 0)  and (Itemname = :ten or :ten='') and (GrpGoodsId = :gg or :gg =0) and (Unit = :dv or :dv=0) or (MadeIn = :xx or :xx=''";
        Query query = se.createQuery(hql);
        query.setParameter("ma", ma);
        query.setParameter("ten", ten);
        query.setParameter("gg", gg);
        query.setParameter("dv", dv);
        query.setParameter("xx", xx);
        List<Items> list = query.list();
        return list;
    }
}
