/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.HibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import pojo.Logsystem;

/**
 *
 * @author tvnam
 */
public class Logsystem_DAL {
    private static final Session se =  HibernateUtil.getSession();
    
    public static List<Logsystem> GetAllLogs()
    {
        Query query = se.createQuery("from Logsystem");
        List<Logsystem> list = query.list();
        return list;
    }
}
