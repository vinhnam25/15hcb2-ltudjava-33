/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serv;

import Utils.DataUserLogin;
import Utils.HibernateUtil;
import Utils.Log;
import static java.lang.Math.toIntExact;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import org.hibernate.Query;
import org.hibernate.Session;
import pojo.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Font;
/**
 *
 * @author TNam
 */
public class SerivceData {
    
    private static final Session se =  HibernateUtil.getSession();
    
    public static List<Object[]> GetProductsByIDWhs(String id)
    {
        List<Object[]> ls = null;
        try
        {
            String hql = "from Inventorys t, Items i  where t.itemCode = i.itemCode and t.whsCode = '" + id + "'";
            Query q = se.createQuery(hql);
            ls = q.list();         
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetProductsByIDWhs()", ex.getMessage() );
            l.Write_Log_Local();
            ex.toString();
        }
        return ls;
    }
    
    public static Units GetUnitByID(int id)
    {
        Units u = null;
        try
        {
            String hql = "from Units u where u.id = :id";
            Query q = se.createQuery(hql);
            q.setParameter("id", id);
            List<Units> l = q.list();         
            u = l.get(0);
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetUnitByID()", ex.getMessage() );
            l.Write_Log_Local();
            ex.toString();
        }
        return u;
    }
    
    public static List<Customers> GetDataCustomer()
    {
        List<Customers> ls = null;
        try
        {
            String hql = "from Customers d where d.status = 'A'";
            Query q = se.createQuery(hql);
            ls = q.list();         
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetDataCustomer()", ex.getMessage() );
            l.Write_Log_Local();
            ex.toString();
        }
        return ls;
    }
    
    public static List<Distributors> GetDataDistribute()
    {
        List<Distributors> ls = null;
        try
        {
            String hql = "from Distributors d where d.status = 'A'";
            Query q = se.createQuery(hql);
            ls = q.list();         
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetDataDistribute()", ex.getMessage() );
            l.Write_Log_Local();
            ex.toString();
        }
        return ls;
    }
    
    public static Customers GetDataCustomerById(int id)
    {
        Customers obj = null;
        try
        {
            String hql = "from Customers c where c.customerId = :id";
            Query q = se.createQuery(hql);
            q.setParameter("id", id);
            List<Customers> ls = q.list();  
            obj = ls.get(0);
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetDataCustomerById()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        return obj;
    }
    
    public static List<Object[]> SearchItem(String id, String name)
    {
        List<Object[]> obj = null;
        if(id.isEmpty())
        {
            id = "c.itemCode";
        }
        try
        {
            String hql = "from Items c, Inventorys i where c.itemCode = i.itemCode and c.whsCode = i.whsCode and c.itemCode = "+id+" and c.itemName Like '%" + name + "%'";
            Query q = se.createQuery(hql);
            obj = q.list();  
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"SearchItem()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        return obj;
    }
    
    public static SaleDetail GetItemByID(String id)
    {
        SaleDetail obj = null;
        try
        {
            //select c.itemCode, c.itemName, u.name, c.whsCode, DATE_FORMAT(c.UDateUse, '%d-%m-%Y'), c.priceSale, c.perTax 
            String hql = "from Items c, Units u where c.unit = u.id and c.itemCode = '"+id+"'";
            List<Object[]> ls = se.createQuery(hql).list();
            for (Object[] aRow : ls) {
                Items i = (Items) aRow[0];
                Units u = (Units) aRow[1];
                
                obj = new SaleDetail (
                        i.getItemCode(),
                        i.getItemName(),
                        u.getName(),
                        i.getWhsCode(),
                        i.getUDateUse(),
                        i.getPriceSale(),
                        i.getPerTax()                  
                );
                break;
            }
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetItemByID()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        return obj;
    }
    
    public static SaleDetail GetItemByID(String id, String makho)
    {
        SaleDetail obj = null;
        try
        {
            //select c.itemCode, c.itemName, u.name, c.whsCode, DATE_FORMAT(c.UDateUse, '%d-%m-%Y'), c.priceSale, c.perTax 
            String hql = "from Items c, Units u where c.unit = u.id and c.whsCode = '"+ makho +"' and c.itemCode = '"+id+"'";
            List<Object[]> ls = se.createQuery(hql).list();
            for (Object[] aRow : ls) {
                Items i = (Items) aRow[0];
                Units u = (Units) aRow[1];
                
                obj = new SaleDetail (
                        i.getItemCode(),
                        i.getItemName(),
                        u.getName(),
                        i.getWhsCode(),
                        i.getUDateUse(),
                        i.getPriceSale(),
                        i.getPerTax()                  
                );
                break;
            }
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetItemByID()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        return obj;
    }
    
    public static int SaveInfoSaleForCustomer(int customerId, DefaultTableModel md, String user, Long tongTT)
    {
        int kq = 0;
        try
        {
            
            Dealheader dh = new Dealheader();
            dh.setDocType(1);
            dh.setDocStatus("O");
            dh.setDistCode(0);
            dh.setCustomerId(customerId);
            dh.setNote("Bán hàng");
            dh.setCreateDate(new Date());
            dh.setCreateBy(user);
            dh.setTotalPrice(tongTT);
            
            se.beginTransaction();
            kq = (int)se.save(dh);
            
            int docentry = dh.getDocEntry();
            
            //Save details 
            if(kq > 0)
            {
                int rowNumber = md.getRowCount();
                for(int i = 0; i < rowNumber; i++)
                {
                    Dealdetails dl = new Dealdetails();
                    dl.setDocEntry(docentry);
                    dl.setItemCode((String)md.getValueAt(i, 0));
                    dl.setQuantity((int)md.getValueAt(i, 6));
                    String donGia = (String)md.getValueAt(i, 5);
                    donGia = donGia.replace(",", "");
                    dl.setUPrice(Long.valueOf(donGia));
                    dl.setWhsCode((String)md.getValueAt(i, 3));
                    dl.setUTmoney(dl.getUPrice() * dl.getQuantity());
                    double tax = Double.valueOf(md.getValueAt(i, 8).toString());
                    dl.setTaxPrice(Long.valueOf((int)tax));
                    dl.setTotalCost(Long.valueOf(md.getValueAt(i, 10).toString().replace(",", "")));
                    
                    se.save(dl);
                    
                }
                
            }                    
            
            se.getTransaction().commit();
            System.out.println(kq);
            kq = docentry;
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"SaveInfoSaleForCustomer()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        return kq;
    }
    
    public static int SaveInfoSaleForDistributor(int discode, DefaultTableModel md, String user, Long tongTT)
    {
        int kq = 0;
        try
        {

            Dealheader dh = new Dealheader();
            dh.setDocType(2);
            dh.setDocStatus("O");
            dh.setDistCode(0);
            dh.setDistCode(discode);
            dh.setNote("Mua hàng");
            dh.setCreateDate(new Date());
            dh.setCreateBy(user);
            dh.setTotalPrice(tongTT);
            
            se.beginTransaction();
            kq = (int)se.save(dh);
            
            int docentry = dh.getDocEntry();
            
            //Save details 
            if(kq > 0)
            {
                int rowNumber = md.getRowCount();
                for(int i = 0; i < rowNumber; i++)
                {
                    Dealdetails dl = new Dealdetails();
                    dl.setDocEntry(docentry);
                    dl.setItemCode((String)md.getValueAt(i, 0));
                    dl.setQuantity((int)md.getValueAt(i, 6));
                    String donGia = (String)md.getValueAt(i, 5);
                    donGia = donGia.replace(",", "");
                    dl.setUPrice(Long.valueOf(donGia));
                    dl.setWhsCode((String)md.getValueAt(i, 3));
                    dl.setUTmoney(dl.getUPrice() * dl.getQuantity());
                    double tax = Double.valueOf(md.getValueAt(i, 8).toString());
                    dl.setTaxPrice(Long.valueOf((int)tax));
                    dl.setTotalCost(Long.valueOf(md.getValueAt(i, 10).toString().replace(",", "")));
                    
                    se.save(dl);
                    
                }
                
            }                    
            
            se.getTransaction().commit();
            System.out.println(kq);
            kq = docentry;
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"SaveInfoSaleForDistributor()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        return kq;
    }
    
    public static int GetValuePrimaryKey(String tableName, String colName)
    {
        int row = -1;
        try
        {
            String hql = "select count(d." + colName + ") from "+ tableName +" d";
            long r = (long)se.createQuery(hql).uniqueResult();
            row = toIntExact(r);
            row = row + 1;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetValuePrimaryKey()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        return row;
    }
    
    public static int SaleCancel(int id, String user)
    {
        int kq = 0;

        se.beginTransaction();
        try
        {         
            
            String hql = "from Dealheader c where c.docEntry = :id";
            Dealheader ls = (Dealheader)se.createQuery(hql).setParameter("id", id).uniqueResult();
            ls.setDocStatus("C");
            ls.setUpdateBy(user);
            se.update(ls);
            
            kq = 1;
            se.getTransaction().commit();
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"SaleCancel()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;          
        }
        return kq;
    }
    
    public static int SaleFinish(int id, Long cash, String user)
    {
        int kq = 0;
        List<Dealdetails> list = null;
        se.beginTransaction();
        try
        {                  
            String hql = "from Dealheader c where c.docEntry = :id";
            Dealheader ls = (Dealheader)se.createQuery(hql).setParameter("id", id).uniqueResult();
            ls.setDocStatus("F");
            ls.setUpdateDate(new Date());
            ls.setUpdateBy(user);
            se.update(ls);
            
            hql = "from Dealdetails c where c.docEntry = :id";
            list = se.createQuery(hql).setParameter("id", id).list();
            for(Dealdetails v : list)
            {
                //trừ kho
                String masp = v.getItemCode();
                String makho = v.getWhsCode();
                hql = "from Inventorys i where i.itemCode = " + masp + " and i.whsCode = " + makho;
                Inventorys obj = (Inventorys)se.createQuery(hql).uniqueResult(); 
                obj.setOnHand(obj.getOnHand() - v.getQuantity());                

                se.update(obj);
            }
            
            //phát sinh chứng từ thu tiền
            Bills ct = new Bills();
            ct.setCreateDate(new Date());
            ct.setCreateBy("-1");
            ct.setDocEntrySo(id);
            ct.setUMoCash(cash);
            ct.setUDebt(cash);
            ct.setUPay(Long.valueOf("0"));
            ct.setStatus("O");
            ct.setDocType("T");
            
            se.save(ct);
            kq = ct.getDocEntry();
            se.getTransaction().commit();
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"SaleFinish()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;          
        }
        return kq;
    }
    
    public static int SaleFinishDistributor (int id, Long cash, String user)
    {
        int kq = 0;
        List<Dealdetails> list = null;
        se.beginTransaction();
        try
        {                  
            String hql = "from Dealheader c where c.docEntry = :id";
            Dealheader ls = (Dealheader)se.createQuery(hql).setParameter("id", id).uniqueResult();
            ls.setDocStatus("F");
            ls.setUpdateBy(user);
            ls.setUpdateDate(new Date());
            se.update(ls);
            
            hql = "from Dealdetails c where c.docEntry = :id";
            list = se.createQuery(hql).setParameter("id", id).list();
            for(Dealdetails v : list)
            {
                //tăng kho
                String masp = v.getItemCode();
                String makho = v.getWhsCode();
                hql = "from Inventorys i where i.itemCode = " + masp + " and i.whsCode = " + makho;
                Inventorys obj = (Inventorys)se.createQuery(hql).uniqueResult(); 
                obj.setOnHand(obj.getOnHand() + v.getQuantity());

                se.update(obj);
            }
            
            //phát sinh chứng từ chi tiền
            Bills ct = new Bills();
            ct.setCreateDate(new Date());
            ct.setCreateBy("-1");
            ct.setDocEntrySo(id);
            ct.setUMoCash(cash);
            ct.setUDebt(cash);
            ct.setUPay(Long.valueOf("0"));
            ct.setStatus("O");
            ct.setDocType("C");
            
            se.save(ct);
            kq = ct.getDocEntry();
            se.getTransaction().commit();
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"SaleFinishDistributor()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;          
        }
        return kq;
    }
    
    public static List<Bills> SearchBills (int IDThuTien, int DocSO, Date ngaytao, String loaiCT)
    {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        String nt = dt.format(ngaytao);
        List<Bills> ls = null;
        try
        {
            String hql = "from Bills b where (b.docEntry = :id or :id = 0) and b.docType = '" + loaiCT + "'" +
                            " and (b.docEntrySo = :docSo or :docSo = 0) and DATE(b.createDate) = '" + nt + "'" ;
            Query q = se.createQuery(hql);
            q.setParameter("id", IDThuTien);
            q.setParameter("docSo", DocSO);
            
            ls = q.list();
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"SearchBills()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        return ls;
    }
    
    public static int UpdateBills (int IDThuTien, Long cash)
    {
        int kq = 0;
        try
        {
            se.beginTransaction();
            
            String hql = "from Bills b where b.docEntry = :id";
            Bills rs = (Bills)se.createQuery(hql).setParameter("id", IDThuTien).uniqueResult();
            
            rs.setUPay(rs.getUPay() + cash);
            rs.setUDebt(rs.getUDebt() - cash);
            if(rs.getUDebt() == 0) rs.setStatus("F");
            else rs.setStatus("W");
            
            se.update(rs);
            kq = 1;
            se.getTransaction().commit();
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"UpdateBills()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        return kq;
    }
    
    public static List<Useraccount> SignIn(String username, String password)
    {
        List<Useraccount> obj = null;        
        try
        {
            String hql = "from Useraccount u where u.username = :username and u.password = :password";
            System.out.println(hql);
            Query q = se.createQuery(hql);
            q.setParameter("username", username);
            q.setParameter("password", password);
            obj = q.list();         

        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"SignIn()", ex.getMessage() );
            l.Write_Log_Local();
            ex.toString();
        }
        
        return obj;
    }
     
    //Service load thong tin chung tu
    public static List<Bills> SearchBillData(int docEntry, String type, Date fromDate, Date toDate, String Status)
    {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        String frDate = dt.format(fromDate);
        String tDate = dt.format(toDate);

        List<Bills> res = null;
        
        if(type.equals("Tất cả")) type = "A";
        else if(type.equals("Phiếu thu")) type = "T";
        else type = "C";
        
        if(Status.equals("Tất cả")) Status = "A";
        else if(Status.equals("Mở")) Status = "O";
        else if (Status.equals("Đang nợ")) Status = "W";
        else Status = "F";
        
        try
        {
            String hql = "from Bills b where ( b.docEntry = :id or :id = 0) and (b.docType = '"+type +"' or '"+type +"' = 'A') and (b.status = '"+Status +"' or '"+Status +"' = 'A') and " +
                        "DATE(b.createDate) between '"+frDate +"' and '"+tDate +"' ";
            Query q = se.createQuery(hql);
            q.setParameter("id", docEntry);
            
            res = q.list();
                      
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"SearchBillData()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        
        return res;
    }
    
    public static ArrayList<DealHistory> GetDataHistoryDeal(String mahang, String makho, String tuNgay, String denNgay, int idgd)
    {
        ArrayList<DealHistory> ls = new ArrayList<>();
        try
        {
            String hql = "from Dealheader a, Dealdetails b, Items i, Warehouses w " + 
                        "where a.docEntry = b.docEntry and b.itemCode = i.itemCode and b.whsCode = w.whsCode and " + 
                        "DATE(a.createDate) between '" + tuNgay + "' and '"+ denNgay +"' and ( b.itemCode = '" + mahang +"' or '" + mahang +"' = 'A') and " +
                        " ( b.whsCode = '" + makho +"' or '" + makho +"' = 'A') and (a.docEntry = :magd or :magd = 0) ";
            List<Object[]> rs = se.createQuery(hql).setParameter("magd", idgd).list();
            for(Object[] row : rs)
            {
                Dealheader a = (Dealheader)row[0];
                Dealdetails b = (Dealdetails)row[1];
                Items i = (Items)row[2];
                Warehouses w = (Warehouses)row[3];
                
                DealHistory e = new DealHistory();
                e.setDocEntry(a.getDocEntry());
                e.setDocType(a.getDocType());
                e.setDocStatus(a.getDocStatus());
                e.setItemCode(b.getItemCode());
                e.setItemName(i.getItemName());
                e.setNgayTao(a.getCreateDate());
                e.setWhsCode(b.getWhsCode());
                e.setWhsName(w.getWhsName());
                e.setQuantity(b.getQuantity());
                e.setDonGia(b.getUPrice());
                e.setThanhTien(b.getUTmoney());
                e.setChuThich(a.getNote());
                
                ls.add(e);
            }
                       
        }
        catch (Exception ex)
        {
            Log l = new Log(new Date(),"GetDataHistoryDeal()", ex.getMessage() );
            l.Write_Log_Local();
            ls = null;
            throw ex;
        }
        
        return ls;
    }
    
    public static List<Items> GetAllItem()
    {
        List<Items> ls = null;
        try
        {
            String hql = "from Items";
            ls = se.createQuery(hql).list();
        }
        catch (Exception ex)
        {
            Log l = new Log(new Date(),"GetAllItem()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        
        return ls;
    }
    
    public static List<Warehouses> GetAllWhs()
    {
        List<Warehouses> ls = null;
        try
        {
            String hql = "from Warehouses";
            ls = se.createQuery(hql).list();
        }
        catch (Exception ex)
        {
            Log l = new Log(new Date(),"GetAllWhs()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        
        return ls;
    }
    
    public static ArrayList<SaleDetail> GetDataDealByID(int DocEntry)
    {
        ArrayList<SaleDetail> ls = null;
        try
        {
            ls = new ArrayList<>();
            String hql = "from Dealdetails d where d.docEntry = :id";
            List<Dealdetails> tems = se.createQuery(hql).setParameter("id", DocEntry).list();
            for(Dealdetails d : tems)
            {
                String itemCode = d.getItemCode();
                SaleDetail e = GetItemByID(itemCode);
                e.setSoLuong(d.getQuantity());
                e.setDonGia(d.getUPrice());
                e.setThanhTien(d.getUTmoney());
                ls.add(e);
            }   
        }
        catch (Exception ex)
        {
            Log l = new Log(new Date(),"GetDataDealByID()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        
        return ls;
    }
    
    public static Dealheader GetDealHeaderByID (int DocEntry)
    {
        Dealheader result = null;
        try
        {
            String hql = "from Dealheader d where d.docEntry = :id ";
            result = (Dealheader)se.createQuery(hql).setParameter("id", DocEntry).uniqueResult();
        }
        catch (Exception ex)
        {
            Log l = new Log(new Date(),"GetDealHeaderByID()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        
        return result;
    }
    
    public static Customers GetCustomerByID (int DocEntry)
    {
        Customers result = null;
        try
        {
            String hql = "from Customers d where d.customerId = :id ";
            result = (Customers)se.createQuery(hql).setParameter("id", DocEntry).uniqueResult();
        }
        catch (Exception ex)
        {
            Log l = new Log(new Date(),"GetCustomerByID()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        
        return result;
    }
    
    public static Distributors GetDistributorByID (int DocEntry)
    {
        Distributors result = null;
        try
        {
            String hql = "from Distributors d where d.distCode = :id ";
            result = (Distributors)se.createQuery(hql).setParameter("id", DocEntry).uniqueResult();
        }
        catch (Exception ex)
        {
            Log l = new Log(new Date(),"GetDistributorByID()", ex.getMessage() );
            l.Write_Log_Local();
            throw ex;
        }
        
        return result;
    }
    
    public static List<Object[]> GetDataInventory (String masp, String makho)
    {
        try
        {
            String hql = "from Inventorys d, Items i where d.itemCode = i.itemCode and ( d.itemCode = '"+masp+"' or '"+masp+"' = 'A') and (d.whsCode = '"+makho+"' or '"+makho+"' = 'A' ) ";
            List<Object[]> res = se.createQuery(hql).list();
            return res;
        }
        catch (Exception ex)
        {
            Log l = new Log(new Date(),"GetDataInventory()", ex.getMessage() );
            l.Write_Log_Local();
            return null;
        }
    }
    
    public static Warehouses GetWarehouseById(String id)
    {
        Warehouses warehouse = null;
        try
        {
            String hql = "from Warehouses w where w.whsCode = '"+id+"'";
            Query query = se.createQuery(hql);
            List<Warehouses> l = query.list();         
            warehouse = l.get(0);
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetWarehouseById()", ex.getMessage() );
            l.Write_Log_Local();
        }
        return warehouse;
    }
    
    public static ArrayList<SoDuDauKy> ImportDuDauKy (String path)
    {
        ArrayList<SoDuDauKy> s = new ArrayList<SoDuDauKy>();
        try
        {
            FileInputStream file = new FileInputStream(new File(path));

            //Create Workbook instance holding reference to .xlsx file
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            Iterator<Row> rowIterator = sheet.iterator();
            int index = 0;
            while (rowIterator.hasNext()) 
            {
                    Row row = rowIterator.next();
                    if(index == 0) {index = 1; continue;}
                    //For each row, iterate through all the columns
                    SoDuDauKy sd = new SoDuDauKy();
                    sd.setItemCode(row.getCell(0).getStringCellValue());
                    sd.setItemName(row.getCell(1).getStringCellValue());
                    sd.setWhsCode(row.getCell(2).getStringCellValue());
                    sd.setSoLuong((int)row.getCell(3).getNumericCellValue());
                    sd.setDonGia(row.getCell(4).getNumericCellValue());
                    sd.setThanhTien(row.getCell(5).getNumericCellValue());
                    s.add(sd);
            }
            file.close();
            
            return s;
        } 
        catch (Exception ex) 
        {
            Log l = new Log(new Date(),"ImportDuDauKy()", ex.getMessage() );
            l.Write_Log_Local();
                return null;
        }
        
    }
    
    public static int ExportDuLieuDauKy(List<SoDuDauKy> list, String excelFilePath) throws IOException 
    {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        
        createHeaderRow(sheet);
        
        int rowCount = 0;

        for (SoDuDauKy sd : list) {
            Row row = sheet.createRow(++rowCount);
            writeData(sd, row);
        }

        excelFilePath = excelFilePath + "\\IMP_SODUDAUKY_ERROR.xlsx";
        try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
            workbook.write(outputStream);
            
            return 1;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"ExportDuLieuDauKy()", ex.getMessage() );
            l.Write_Log_Local();
            return 0;
        }      
    }
    
    private static void writeData(SoDuDauKy sd, Row row) 
    {
        Cell cell = row.createCell(0);
        cell.setCellValue(sd.getItemCode());

        cell = row.createCell(1);
        cell.setCellValue(sd.getItemName());

        cell = row.createCell(2);
        cell.setCellValue(sd.getWhsCode());
        
        cell = row.createCell(3);
        cell.setCellValue(sd.getSoLuong());
        
        cell = row.createCell(4);
        cell.setCellValue(sd.getDonGia());
        
        cell = row.createCell(5);
        cell.setCellValue(sd.getThanhTien());
        
        cell = row.createCell(6);
        cell.setCellValue(sd.getErr());
        
        cell = row.createCell(7);
        cell.setCellValue("");
    }
    
    private static void createHeaderRow(Sheet sheet) {
 
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.CORAL.index);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        Font font = sheet.getWorkbook().createFont();
        font.setBoldweight((short)5);
        font.setFontHeightInPoints((short) 13);
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);
        Cell cellTitle = row.createCell(0);

        cellTitle.setCellStyle(cellStyle);
        cellTitle.setCellValue("Mã hàng");

        Cell cellAuthor = row.createCell(1);
        cellAuthor.setCellStyle(cellStyle);
        cellAuthor.setCellValue("Tên hàng");

        Cell cellPrice = row.createCell(2);
        cellPrice.setCellStyle(cellStyle);
        cellPrice.setCellValue("Mã kho");
        
        Cell cell_3 = row.createCell(3);
        cell_3.setCellStyle(cellStyle);
        cell_3.setCellValue("Số lượng");
        
        Cell cell_4 = row.createCell(4);
        cell_4.setCellStyle(cellStyle);
        cell_4.setCellValue("Đơn giá");
        
        Cell cell_5 = row.createCell(5);
        cell_5.setCellStyle(cellStyle);
        cell_5.setCellValue("Thành tiền");
        
        Cell cell_6 = row.createCell(6);
        cell_6.setCellStyle(cellStyle);
        cell_6.setCellValue("Nội dung");
        
        Cell cell_7 = row.createCell(7);
        cell_7.setCellValue("");
    }
    
    public static int NhapDuDauKy_Save(List<SoDuDauKy> ls, String createby)
    {
        try
        {
            se.beginTransaction();
            
            //lưu header
            String note = "Nhập dư đầu kỳ";
            Dealheader dh = new Dealheader();
            dh.setCreateBy(createby);
            dh.setCreateDate(new Date());
            dh.setDistCode(0);
            dh.setCustomerId(0);
            dh.setDocStatus("O");
            dh.setDocType(3);
            dh.setNote(note);
            
            se.save(dh);
            
            //lưu details
            for(SoDuDauKy sd : ls)
            {
                Dealdetails dd = new Dealdetails();
                dd.setDocEntry(dh.getDocEntry());
                dd.setItemCode(sd.getItemCode());
                dd.setQuantity(sd.getSoLuong());
                dd.setUPrice(Long.valueOf((int)sd.getDonGia()));
                dd.setWhsCode(sd.getWhsCode());
                dd.setUTmoney(Long.valueOf((int)sd.getThanhTien()));
                
                se.save(dd);
            }
            
            se.getTransaction().commit();
            
            return dh.getDocEntry();
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"NhapDuDauKy_Save()", ex.getMessage() );
            l.Write_Log_Local();
            return 0;
        }
    }
    
     public static int NhapDuDauKy_Finish(int DocEntry, String Updateby)
    {
        try
        {
            se.beginTransaction();
            
            Dealheader ls = (Dealheader)se.get(Dealheader.class, DocEntry);
            ls.setDocStatus("F");
            ls.setUpdateDate(new Date());
            ls.setUpdateBy(Updateby);
            se.update(ls);
            
            String hql = "from Dealdetails c where c.docEntry = :id";
            List<Dealdetails> list = se.createQuery(hql).setParameter("id", DocEntry).list();
            for(Dealdetails v : list)
            {
                //tăng kho
                String masp = v.getItemCode();
                String makho = v.getWhsCode();
                hql = "from Inventorys i where i.itemCode = " + masp + " and i.whsCode = " + makho;
                Inventorys obj = (Inventorys)se.createQuery(hql).uniqueResult(); 
                obj.setOnHand(obj.getOnHand() + v.getQuantity());                

                se.update(obj);
            }
            
            
            se.getTransaction().commit();
            
            return 1;
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"NhapDuDauKy_Finish()", ex.getMessage() );
            l.Write_Log_Local();
            return 0;
        }
    }
     
    public static List<DateUseData> LayThongTinHanSuDung(String masp, String kho, int maloai)
    {
        ArrayList<DateUseData> ls = new ArrayList<DateUseData>();
        try
        {
            String hql = "from Items i, Warehouses w, Goodsgroup g where i.whsCode = w.whsCode and g.ggid = i.grpGoodsId and " +
                        "(i.itemCode = '"+masp+"' or '"+ masp +"' = 'A') and " +
                        "(i.whsCode = '"+kho+"' or '"+ kho +"' = 'A') and " + 
                        "(i.grpGoodsId = :typeId or :typeId = 0)";
            
            List<Object[]> results = se.createQuery(hql).setParameter("typeId", maloai).list();
            for(Object[] r : results)
            {
                Items a = (Items)r[0];
                Warehouses b = (Warehouses)r[1];
                Goodsgroup c = (Goodsgroup)r[2];
                
                DateUseData d = new DateUseData();
                d.setGroupId(c.getGgid());
                d.setGroupName(c.getName());
                d.setItemCode(a.getItemCode());
                d.setItemName(a.getItemName());
                d.setWhsCode(b.getWhsCode());
                d.setWhsName(b.getWhsName());
                d.setRemark(a.getUDesc());
                d.setNgayNhap(a.getUDateIn());
                d.setHanSuDung(a.getUDateUse());
                d.setHanConLai(getDateDiff(new Date(), d.getHanSuDung()));
                
                ls.add(d);
            }
            
            
            return ls;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"LayThongTinHanSuDung()", ex.getMessage() );
            l.Write_Log_Local();
            return null;
        }
    }
    
    private static long getDateDiff(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
    
    public static List<BCDTKhuVuc> LayBCDTTheoKhuVuc(int makv, Date TuNgay, Date DenNgay)
    {
        ArrayList<BCDTKhuVuc> ls = new ArrayList<BCDTKhuVuc>();
        try
        {
            SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
            String tuNgay = dt.format(TuNgay);
            String denNgay = dt.format(DenNgay);

            String hql = "from Regions r, Customers c, Dealheader d, Dealdetails t, Items i where d.docStatus = 'F' and " +
                        "r.regionId = c.region and c.customerId = d.customerId and d.docEntry = t.docEntry and t.itemCode = i.itemCode and " +
                        "(r.regionId = :id or :id = 0) and DATE(d.createDate) between '"+tuNgay+"' and '"+denNgay+"'";
            Query q = se.createQuery(hql).setParameter("id", makv);
            List<Object[]> ds = q.list();
            
            for(Object[] row : ds)
            {
                Regions r = (Regions)row[0];
                Customers c = (Customers)row[1];
                Dealheader d = (Dealheader)row[2];
                Dealdetails t = (Dealdetails)row[3];
                Items i = (Items)row[4];
                
                String masp = t.getItemCode();
                int idkv = r.getRegionId();
                
                int index = HasExistElement_BC(ls, masp, idkv);
                
                if(index >= 0)
                {
                    int sl = ls.get(index).getSoLuong();
                    int v = t.getQuantity();
                    ls.get(index).setSoLuong(sl + v);
                    
                    long dthu = ls.get(index).getTongDoanhThu();
                    long v_dthu = t.getTotalCost();
                    ls.get(index).setTongDoanhThu(dthu+v_dthu);
                }
                else
                {
                    BCDTKhuVuc b = new BCDTKhuVuc();
                    b.setItemCode(i.getItemCode());
                    b.setItemName(i.getItemName());
                    b.setMaKV(r.getRegionId());
                    b.setKhuvuc(r.getName());
                    b.setSoLuong(t.getQuantity());
                    b.setTongDoanhThu(t.getTotalCost());

                    ls.add(b);
                }
            }
            return ls;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"LayBCDTTheoKhuVuc()", ex.getMessage() );
            l.Write_Log_Local();
            return null;
        }
    }
    
    public static int ExportDuLieuBCDTKhuVuc(List<BCDTKhuVuc> list, String excelFilePath) throws IOException 
    {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        
        createHeaderRow_1(sheet);
        
        int rowCount = 0;

        for (BCDTKhuVuc sd : list) {
            Row row = sheet.createRow(++rowCount);
            writeData(sd, row);
        }

        excelFilePath = excelFilePath + "\\BCDT_TheoKhucVuc.xlsx";
        try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
            workbook.write(outputStream);
            return 1;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"ExportDuLieuBCDTKhuVuc()", ex.getMessage() );
            l.Write_Log_Local();
            return 0;
        }      
    }
    
    private static void writeData(BCDTKhuVuc sd, Row row) 
    {
        Cell cell = row.createCell(0);
        cell.setCellValue(sd.getKhuvuc());

        cell = row.createCell(1);
        cell.setCellValue(sd.getItemCode());

        cell = row.createCell(2);
        cell.setCellValue(sd.getItemName());
        
        cell = row.createCell(3);
        cell.setCellValue(sd.getSoLuong());
        
        cell = row.createCell(4);
        cell.setCellValue(sd.getLoaiTien());
        
        cell = row.createCell(5);
        cell.setCellValue(sd.getTongDoanhThu());
        
    }
    
    private static void createHeaderRow_1(Sheet sheet) {
 
        CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
        cellStyle.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        Font font = sheet.getWorkbook().createFont();
        font.setBoldweight((short)5);
        font.setFontHeightInPoints((short) 13);
        cellStyle.setFont(font);

        Row row = sheet.createRow(0);
        Cell cellTitle = row.createCell(0);

        cellTitle.setCellStyle(cellStyle);
        cellTitle.setCellValue("Khu vực");

        Cell cellAuthor = row.createCell(1);
        cellAuthor.setCellStyle(cellStyle);
        cellAuthor.setCellValue("Mã hàng");
        
        Cell itemName = row.createCell(2);
        itemName.setCellStyle(cellStyle);
        itemName.setCellValue("Tên hàng");

        Cell cellPrice = row.createCell(3);
        cellPrice.setCellStyle(cellStyle);
        cellPrice.setCellValue("Số lượng");
        
        Cell cell_3 = row.createCell(4);
        cell_3.setCellStyle(cellStyle);
        cell_3.setCellValue("Loại tiền");
        
        Cell cell_4 = row.createCell(5);
        cell_4.setCellStyle(cellStyle);
        cell_4.setCellValue("Doanh thu");
        
        Cell cell_5 = row.createCell(6);
        cell_5.setCellValue("");
        
    }
    
    public static List<THTonBanHang> GetDataTBH()
    {
        try
        {
            ArrayList<THTonBanHang> ls = new ArrayList<THTonBanHang>();
            
            String hql = "from Dealheader d, Dealdetails t, Items i where d.docEntry = t.docEntry and t.itemCode = i.itemCode";
            List<Object[]> rs = se.createQuery(hql).list();
            
            for(Object[] row : rs)
            {
                THTonBanHang th = new THTonBanHang();
                Dealheader d = (Dealheader)row[0];
                Dealdetails t = (Dealdetails)row[1];
                Items i = (Items)row[2];
                
                String masp = t.getItemCode();
                
                int index = HasExistElement(ls, masp);               
                       
                int type = d.getDocType();
                
                switch(type)
                {
                    case 1:
                    {
                       if(index >= 0)
                       {
                           int sl = ls.get(index).getSoLuongXK();
                           Long tt = ls.get(index).getThanhTienXK();
                           ls.get(index).setSoLuongXK(sl + t.getQuantity());
                           ls.get(index).setThanhTienXK(tt + t.getUTmoney());
                       }
                       else
                       {
                           th.setMaHang(masp);
                           th.setTenHang(i.getItemName());
                           th.setSoLuongXK(t.getQuantity());
                           th.setThanhTienXK(t.getUTmoney());
                           ls.add(th);
                       }
                    }
                    break;
                        
                    case 2:
                    {
                       if(index >= 0)
                       {
                           int sl = ls.get(index).getSoLuongNK();
                           Long tt = ls.get(index).getThanhTienNK();
                           ls.get(index).setSoLuongNK(sl + t.getQuantity());
                           ls.get(index).setThanhTienNK(tt + t.getUTmoney());
                       }
                       else
                       {
                           th.setMaHang(masp);
                           th.setTenHang(i.getItemName());
                           th.setSoLuongNK(t.getQuantity());
                           th.setThanhTienNK(t.getUTmoney());
                           ls.add(th);
                       }
                    }
                    break;
                        
                    case 3:
                    {
                       if(index >= 0)
                       {
                           int sl = ls.get(index).getSoLuongDK();
                           Long tt = ls.get(index).getThanhTienDK();
                           ls.get(index).setSoLuongDK(sl + t.getQuantity());
                           ls.get(index).setThanhTienDK(tt + t.getUTmoney());
                       }
                       else
                       {
                           th.setMaHang(masp);
                           th.setTenHang(i.getItemName());
                           th.setSoLuongDK(t.getQuantity());
                           th.setThanhTienDK(t.getUTmoney());
                           ls.add(th);
                       }
                    }
                    break;

                }
            }
            
            return ls;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetDataTBH()", ex.getMessage() );
            l.Write_Log_Local();
            return null;
        }
    }
    
    private static int HasExistElement(List<THTonBanHang> ls, String msp)
    {
        for(int i = 0 ; i < ls.size(); i++)
        {
            if(ls.get(i).getMaHang().equals(msp))
            {
                return i;
            }
        }
        
        return -1;
    }
    
    private static int HasExistElement_BC(List<BCDTKhuVuc> ls, String msp, int idkv)
    {
        for(int i = 0 ; i < ls.size(); i++)
        {
            if(ls.get(i).getItemCode().equals(msp) && ls.get(i).getMaKV() == idkv)
            {
                return i;
            }
        }
        
        return -1;
    }
    
    public static int SaveHistorySystem (String tenChucNang, String hanhDong, String Obj)
    {
        try
        {
            se.beginTransaction();
            Logsystem ls = new Logsystem();
            ls.setSUser(DataUserLogin.user);
            ls.setPc("[" + DataUserLogin.ip + "] " + DataUserLogin.hostname);
            ls.setTimeAction(new Date());
            ls.setFunction(tenChucNang);
            ls.setAction(hanhDong);
            ls.setObjType(Obj);
            
            se.save(ls);
            se.getTransaction().commit();
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"SaveHistorySystem()", ex.getMessage() );
            l.Write_Log_Local();
        }
        
        return 1;
    }
    
    public static List<Logsystem> GetHistorySystem ()
    {
        ArrayList<Logsystem> ls = new ArrayList<Logsystem>();
        try
        {
            String hql = "from Logsystem";
            List<Logsystem> rs = se.createQuery(hql).list();
            
            return rs;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetHistorySystem()", ex.getMessage() );
            l.Write_Log_Local();
            return null;
        }
    }
    
    public static List<ListFunction> GetListFunction ()
    {

        try
        {
            String hql = "from ListFunction";
            List<ListFunction> rs = se.createQuery(hql).list();
            
            return rs;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetListFunction()", ex.getMessage() );
            l.Write_Log_Local();
            return null;
        }
    }
    
    public static List<PermitsTable> GetFunctionByUserID (int UserID)
    {
        try
        {
            String hql = "from PermitsTable p where p.userId = :id";
            List<PermitsTable> rs = se.createQuery(hql).setParameter("id", UserID).list();
            
            return rs;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetFunctionByUserID()", ex.getMessage() );
            l.Write_Log_Local();
            return null;
        }
    }
    
    public static int SaveFunctionPermits (List<PermitsTable> ls)
    {
        try
        {
            se.beginTransaction();
            for(PermitsTable i : ls)
            {
                PermitsTable pt = (PermitsTable)se.get(PermitsTable.class, i.getId());
                pt.setStatus(i.getStatus());
                se.update(pt);
            }

            se.getTransaction().commit();
            
            return 1;
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"SaveFunctionPermits()", ex.getMessage() );
            l.Write_Log_Local();
            return 0;
        }
    }
    
    public static List<Useraccount> GetAllUserID ()
    {
        try
        {
            String hql = "from Useraccount p";
            List<Useraccount> rs = se.createQuery(hql).list();
            
            return rs;
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"GetAllUserID()", ex.getMessage() );
            l.Write_Log_Local();
            return null;
        }
    }
    
    public static int ChuyenKho_TaoXuatKho(DefaultTableModel dm, String user, String khoxuat)
    {
        try
        {
            se.beginTransaction();
            
            Dealheader dh = new Dealheader();
            dh.setCreateBy(user);
            dh.setDistCode(0);
            dh.setCustomerId(0);
            dh.setCreateDate(new Date());
            dh.setDocStatus("O");
            dh.setDocType(1);
            dh.setTotalPrice((long)0);
            dh.setNote("Xuất chuyển kho");
            
            se.save(dh);
            
            int soDong = dm.getRowCount();
            
            for(int i = 0; i < soDong; i++)
            {
                Dealdetails dl = new Dealdetails();
                dl.setDocEntry(dh.getDocEntry());
                dl.setItemCode(dm.getValueAt(i, 0).toString());
                dl.setQuantity((int)dm.getValueAt(i, 2));
                dl.setWhsCode(khoxuat);
                dl.setTaxPrice((long)0);
                dl.setTotalCost((long)0);
                dl.setUPrice((long)0);
                dl.setUTmoney((long)0);
                
                se.save(dl);
            }
            
            se.getTransaction().commit();
            
            return dh.getDocEntry();
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"ChuyenKho_TaoXuatKho()", ex.getMessage() );
            l.Write_Log_Local();
            return 0;
        }
    }
    
    public static int ChuyenKho_TaoNhapKho(DefaultTableModel dm, String user, String khonhap)
    {
        try
        {
            se.beginTransaction();
            
            Dealheader dh = new Dealheader();
            dh.setCreateBy(user);
            dh.setDistCode(0);
            dh.setCustomerId(0);
            dh.setCreateDate(new Date());
            dh.setDocStatus("O");
            dh.setNote("Nhập chuyển kho");
            dh.setDocType(2);
            dh.setTotalPrice((long)0);
            
            se.save(dh);
            
            int soDong = dm.getRowCount();
            
            for(int i = 0; i < soDong; i++)
            {
                Dealdetails dl = new Dealdetails();
                dl.setDocEntry(dh.getDocEntry());
                dl.setItemCode(dm.getValueAt(i, 0).toString());
                dl.setQuantity((int)dm.getValueAt(i, 2));
                dl.setWhsCode(khonhap);
                dl.setTaxPrice((long)0);
                dl.setTotalCost((long)0);
                dl.setUPrice((long)0);
                dl.setUTmoney((long)0);
                
                se.save(dl);
            }
            
            se.getTransaction().commit();
            
            return dh.getDocEntry();
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"ChuyenKho_TaoNhapKho()", ex.getMessage() );
            l.Write_Log_Local();
            return 0;
        }
    }
    
    public static int HoanTatChuyenKho(int SoXK, int SoNK, String user)
    {
        try
        {
            se.beginTransaction();
            
            Dealheader dh = (Dealheader)se.get(Dealheader.class, SoXK);
            dh.setDocStatus("F");
            dh.setUpdateBy(user);
            dh.setUpdateDate(new Date());
            
            se.update(dh);
            
            String hql = "from Dealdetails c where c.docEntry = :id";
            List<Dealdetails> list = se.createQuery(hql).setParameter("id", SoXK).list();
            for(Dealdetails v : list)
            {
                //trừ kho
                String masp = v.getItemCode();
                String makho = v.getWhsCode();
                hql = "from Inventorys i where i.itemCode = " + masp + " and i.whsCode = " + makho;
                Inventorys obj = (Inventorys)se.createQuery(hql).uniqueResult(); 
                obj.setOnHand(obj.getOnHand() - v.getQuantity());                

                se.update(obj);
            }
            
            Dealheader dh_n = (Dealheader)se.get(Dealheader.class, SoNK);
            dh_n.setDocStatus("F");
            dh_n.setUpdateBy(user);
            dh_n.setUpdateDate(new Date());
            
            se.update(dh_n);
            
            hql = "from Dealdetails c where c.docEntry = :id";
            list = se.createQuery(hql).setParameter("id", SoNK).list();
            for(Dealdetails v : list)
            {
                //tang kho
                String masp = v.getItemCode();
                String makho = v.getWhsCode();
                hql = "from Inventorys i where i.itemCode = " + masp + " and i.whsCode = " + makho;
                Inventorys obj = (Inventorys)se.createQuery(hql).uniqueResult(); 
                
                if(obj == null)
                {
                    Inventorys i = new Inventorys();
                    i.setWhsCode(makho);
                    i.setItemCode(masp);
                    i.setOnHand(v.getQuantity());
                    i.setInvMin(1);
                    
                    se.save(i);
                }
                else
                {               
                    obj.setOnHand(obj.getOnHand() + v.getQuantity());                
                    se.update(obj);
                }
            }
            
            se.getTransaction().commit();
            
            return 1;
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"HoanTatChuyenKho()", ex.getMessage() );
            l.Write_Log_Local();
            return 0;
        }
    }
    
    public static int HuyChuyenKho(int SoXK, int SoNK, String user)
    {
        try
        {
            se.beginTransaction();
            
            Dealheader dh = (Dealheader)se.get(Dealheader.class, SoXK);
            dh.setDocStatus("C");
            dh.setUpdateBy(user);
            dh.setUpdateDate(new Date());
            
            se.update(dh);
            
            Dealheader dh_n = (Dealheader)se.get(Dealheader.class, SoNK);
            dh_n.setDocStatus("C");
            dh_n.setUpdateBy(user);
            dh_n.setUpdateDate(new Date());
            
            se.update(dh_n);
           
            
            se.getTransaction().commit();
            
            return 1;
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"HuyChuyenKho()", ex.getMessage() );
            l.Write_Log_Local();
            return 0;
        }
    }
    
    public static int TaoPhanQuyen(int userid)
    {
        try
        {
            se.beginTransaction();
            
            String hql_1 = "from PermitsTable p where p.userId = :id";
            List<PermitsTable> rr  = se.createQuery(hql_1).setParameter("id", userid).list();
            
            if(rr.size() > 0)
            {
                se.getTransaction().commit();
                return -1; //đã có phân quyền
            }
            
            String hql = "from ListFunction";
            List<ListFunction> ls  = se.createQuery(hql).list();
            
            for(ListFunction l : ls)
            {
                PermitsTable pb = new PermitsTable();
                pb.setUserId(userid);
                pb.setIdFunction(l.getId());
                pb.setName(l.getName());
                pb.setStatus(0);
                
                se.save(pb);
            }
            
            se.getTransaction().commit();
            return 1;// tạo phân quyền thành công
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"TaoPhanQuyen()", ex.getMessage() );
            l.Write_Log_Local();
            return 0; //lỗi
        }
    }
    
    public static List<TyGia> LayDanhSachTyGia()
    {
        try
        {           
            String hql_1 = "from TyGia ";
            List<TyGia> rr  = se.createQuery(hql_1).list();
            
            
            return rr;// tạo phân quyền thành công
        }
        catch(Exception ex)
        {
            Log l = new Log(new Date(),"LayDanhSachTyGia()", ex.getMessage() );
            l.Write_Log_Local();
            return null; //lỗi
        }
    }
    
    public static int ThemHoacCapNhatTyGia(int type, TyGia tg)
    {
        try
        {           
            se.beginTransaction();
            if(type == 1)
            {
                se.save(tg);
            }
            else if(type == 0)
            {
                TyGia t = (TyGia)se.get(TyGia.class, tg.getMaTG());
                t.setQuocGia(tg.getQuocGia());
                t.setStatus(tg.getStatus());
                t.setTenTG(tg.getTenTG());
                t.setQuyDoi(tg.getQuyDoi());
                
                se.update(t);
            }
            else
            {
                TyGia t = (TyGia)se.get(TyGia.class, tg.getMaTG());
                se.delete(t);
            }
            
            se.getTransaction().commit();
            return 1;// tạo phân quyền thành công
        }
        catch(Exception ex)
        {
            se.getTransaction().rollback();
            Log l = new Log(new Date(),"ThemHoacCapNhatTyGia() " + String.valueOf(type), ex.getMessage() );
            l.Write_Log_Local();
            return 0; //lỗi
        }
    }
}
