/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import static org.apache.poi.hssf.usermodel.HeaderFooter.file;

/**
 *
 * @author TNam
 */
public class Log {
    private Date date;
    private String name;
    private String content;

    public Log() {
    }

    public Log(Date date, String name, String content) {
        this.date = date;
        this.name = "Name : " + name;
        this.content = "Error : " + content;
    }
    
    public void Write_Log_Local()
    {
        SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try 
        {
            //Bước 1: Tạo đối tượng luồng và liên kết nguồn dữ liệu
            FileOutputStream fos = new FileOutputStream("src/data/Log.txt", true);
            PrintStream fw = new PrintStream(fos);

            //Bước 2: Ghi dữ liệu
            String t = dt.format(date);
            fw.println(" ");
            fw.println("Time : "+ t);
            fw.println(name);
            fw.println(content);
            fw.println("--------------------------------------------------------------------------------------");

            //Bước 3: Đóng luồng
            fw.close(); 
        } 
        catch (IOException ex) {
            System.out.println("Loi ghi file: " + ex);
        }
    }
}
