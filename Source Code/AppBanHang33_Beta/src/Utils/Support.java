/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 *
 * @author NamLT3
 */
public class Support {
    
    public static ArrayList<String> readUsingScanner(String fileName) throws IOException {
        ArrayList<String> ls = new ArrayList<String>();
        try 
        {
            File fileDir = new File(fileName);
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF8"));
            String str;

            while ((str = in.readLine()) != null) {
                ls.add(str);
            }

            in.close();
        } catch (UnsupportedEncodingException e) {
            System.out.println(e.getMessage());
            
        } catch (IOException e) {
            System.out.println(e.getMessage());
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            
        }  
        
        return ls;
    }
    
    public static void NhoMatKhau(String ip, String user, String pass, int status)
    {
        try{
         FileOutputStream fos = new FileOutputStream("src/data/Re-pass.txt");
            PrintStream fw = new PrintStream(fos);

            //Bước 2: Ghi dữ liệu
            fw.println(status);
            fw.println(ip);
            fw.println(user);
            fw.println(pass);

            //Bước 3: Đóng luồng
            fw.close(); 
        }
        catch(Exception ex)
        {
            ex.toString();
        }
    }
    
    public static ArrayList<String> LoadMatKhau()
    {
         ArrayList<String> ls = new ArrayList<String>();
        try{
         File fileDir = new File("src/data/Re-pass.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF8"));
            String str;

            while ((str = in.readLine()) != null) {
                ls.add(str);
            }
            
            if(ls.get(0).equals("0")) return null;
            return ls;
        }
        catch(Exception ex)
        {
            ex.toString();
            return null;
        }
    }
}
