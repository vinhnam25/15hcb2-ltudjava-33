/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author TNam
 */
public class THTonBanHang {
    private String maHang;
    private String tenHang;
    private int soLuongDK;
    private Long thanhTienDK;
    private int soLuongXK;
    private Long thanhTienXK;
    private int soLuongNK;
    private Long thanhTienNK;

    public THTonBanHang() {
        soLuongDK = 0;
        thanhTienDK = (long)0;
        soLuongXK = 0;
        thanhTienXK = (long)0;
        soLuongNK = 0;
        thanhTienNK = (long)0;
    }

    public String getMaHang() {
        return maHang;
    }

    public void setMaHang(String maHang) {
        this.maHang = maHang;
    }

    public String getTenHang() {
        return tenHang;
    }

    public void setTenHang(String tenHang) {
        this.tenHang = tenHang;
    }

    public int getSoLuongDK() {
        return soLuongDK;
    }

    public void setSoLuongDK(int soLuongDK) {
        this.soLuongDK = soLuongDK;
    }

    public Long getThanhTienDK() {
        return thanhTienDK;
    }

    public void setThanhTienDK(Long thanhTienDK) {
        this.thanhTienDK = thanhTienDK;
    }

    public int getSoLuongXK() {
        return soLuongXK;
    }

    public void setSoLuongXK(int soLuongXK) {
        this.soLuongXK = soLuongXK;
    }

    public Long getThanhTienXK() {
        return thanhTienXK;
    }

    public void setThanhTienXK(Long thanhTienXK) {
        this.thanhTienXK = thanhTienXK;
    }

    public int getSoLuongNK() {
        return soLuongNK;
    }

    public void setSoLuongNK(int soLuongNK) {
        this.soLuongNK = soLuongNK;
    }

    public Long getThanhTienNK() {
        return thanhTienNK;
    }

    public void setThanhTienNK(Long thanhTienNK) {
        this.thanhTienNK = thanhTienNK;
    }
    
    
}
