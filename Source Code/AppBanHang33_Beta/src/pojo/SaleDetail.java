/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.util.Date;

/**
 *
 * @author TNam
 */
public class SaleDetail {
    
    private String maSP;
    private String tenSP;
    private String dvTinh;
    private String khoHang;
    private Date hanSD;
    private double donGia;
    private int soLuong;
    private double thanhTien;
    private double VAT;
    private double thue;
    private double thanhToan;

    public SaleDetail() {
        soLuong = 1;
        thue = 0;       
    }

    public String getMaSP() {
        return maSP;
    }

    public void setMaSP(String maSP) {
        this.maSP = maSP;
    }

    public String getTenSP() {
        return tenSP;
    }

    public void setTenSP(String tenSP) {
        this.tenSP = tenSP;
    }

    public String getDvTinh() {
        return dvTinh;
    }

    public void setDvTinh(String dvTinh) {
        this.dvTinh = dvTinh;
    }

    public String getKhoHang() {
        return khoHang;
    }

    public void setKhoHang(String khoHang) {
        this.khoHang = khoHang;
    }

    public Date getHanSD() {
        return hanSD;
    }

    public void setHanSD(Date hanSD) {
        this.hanSD = hanSD;
    }

    public double getDonGia() {
        return donGia;
    }

    public void setDonGia(double donGia) {
        this.donGia = donGia;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public double getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(double thanhTien) {
        this.thanhTien = thanhTien;
    }

    public double getVAT() {
        return VAT;
    }

    public void setVAT(double VAT) {
        this.VAT = VAT;
    }

    public double getThue() {
        return thue;
    }

    public void setThue(double thue) {
        this.thue = thue;
    }

    public double getThanhToan() {
        return thanhToan;
    }

    public void setThanhToan(double thanhToan) {
        this.thanhToan = thanhToan;
    }

    public SaleDetail(String maSP, String tenSP, String dvTinh, String khoHang, Date hanSD, double donGia, double VAT) {
        this.maSP = maSP;
        this.tenSP = tenSP;
        this.dvTinh = dvTinh;
        this.khoHang = khoHang;
        this.hanSD = hanSD;
        this.donGia = donGia;
        this.VAT = VAT;
        this.soLuong = 1;
        this.thue = 0;
    }
    
    
}
