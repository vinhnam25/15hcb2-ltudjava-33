/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.util.Date;

/**
 *
 * @author TNam
 */
public class BCDTKhuVuc {
    private int maKV;
    private String khuvuc;
    private String itemCode;
    private String itemName;
    private int soLuong;
    private String loaiTien;
    private String tiGia;
    private Long tongDoanhThu;

    public BCDTKhuVuc() {
        loaiTien = "VNĐ";
        tiGia = "1 VNĐ";
    }

    public int getMaKV() {
        return maKV;
    }

    public void setMaKV(int maKV) {
        this.maKV = maKV;
    }

    public String getKhuvuc() {
        return khuvuc;
    }

    public void setKhuvuc(String khuvuc) {
        this.khuvuc = khuvuc;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public String getLoaiTien() {
        return loaiTien;
    }

    public void setLoaiTien(String loaiTien) {
        this.loaiTien = loaiTien;
    }

    public String getTiGia() {
        return tiGia;
    }

    public void setTiGia(String tiGia) {
        this.tiGia = tiGia;
    }

    public Long getTongDoanhThu() {
        return tongDoanhThu;
    }

    public void setTongDoanhThu(Long tongDoanhThu) {
        this.tongDoanhThu = tongDoanhThu;
    }

    
}
