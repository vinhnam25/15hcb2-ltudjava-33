/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author TNam
 */
public class SearchBills {
    
    private int docEntry;
    private Date ngayTao;
    private String loaiCt;
    private Long thanhTien;
    private String dienGiai;
    private String nguoiTao;

    public SearchBills() {
    }

    public SearchBills(int docEntry, Date ngayTao, String loaiCt, Long thanhTien, String dienGiai, String nguoiTao) {
        this.docEntry = docEntry;
        this.ngayTao = ngayTao;
        this.loaiCt = loaiCt;
        this.thanhTien = thanhTien;
        this.dienGiai = dienGiai;
        this.nguoiTao = nguoiTao;
    }

    public int getDocEntry() {
        return docEntry;
    }

    public void setDocEntry(int docEntry) {
        this.docEntry = docEntry;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getLoaiCt() {
        return loaiCt;
    }

    public void setLoaiCt(String loaiCt) {
        this.loaiCt = loaiCt;
    }

    public Long getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(Long thanhTien) {
        this.thanhTien = thanhTien;
    }

    public String getDienGiai() {
        return dienGiai;
    }

    public void setDienGiai(String dienGiai) {
        this.dienGiai = dienGiai;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }
    
    
}
