/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author TNam
 */
public class TyGia implements java.io.Serializable {
    private Integer maTG;
    private String tenTG;
    private String quocGia;
    private Long quyDoi;
    private String status;

    public TyGia() {
    }

    
    public TyGia(String tenTG, String quocGia, long quydoi, String status) {
        this.tenTG = tenTG;
        this.quocGia = quocGia;
        this.status = status;
        this.quyDoi = quydoi;
    }

    public Integer getMaTG() {
        return maTG;
    }

    public void setMaTG(Integer maTG) {
        this.maTG = maTG;
    }

    public String getTenTG() {
        return tenTG;
    }

    public void setTenTG(String tenTG) {
        this.tenTG = tenTG;
    }

    public String getQuocGia() {
        return quocGia;
    }

    public void setQuocGia(String quocGia) {
        this.quocGia = quocGia;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getQuyDoi() {
        return quyDoi;
    }

    public void setQuyDoi(Long quyDoi) {
        this.quyDoi = quyDoi;
    }
    
    
}
