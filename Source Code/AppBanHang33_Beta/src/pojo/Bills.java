package pojo;
// Generated Sep 24, 2016 4:33:21 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Bills generated by hbm2java
 */
public class Bills  implements java.io.Serializable {


     private Integer docEntry;
     private String docType;
     private Long UMoCash;
     private Date createDate;
     private String createBy;
     private Integer docEntrySo;
     private String status;
     private Long UPay;
     private Long UDebt;

    public Bills() {
    }

    public Bills(String docType, Long UMoCash, Date createDate, String createBy, Integer docEntrySo, String status, Long UPay, Long UDebt) {
       this.docType = docType;
       this.UMoCash = UMoCash;
       this.createDate = createDate;
       this.createBy = createBy;
       this.docEntrySo = docEntrySo;
       this.status = status;
       this.UPay = UPay;
       this.UDebt = UDebt;
    }
   
    public Integer getDocEntry() {
        return this.docEntry;
    }
    
    public void setDocEntry(Integer docEntry) {
        this.docEntry = docEntry;
    }
    public String getDocType() {
        return this.docType;
    }
    
    public void setDocType(String docType) {
        this.docType = docType;
    }
    public Long getUMoCash() {
        return this.UMoCash;
    }
    
    public void setUMoCash(Long UMoCash) {
        this.UMoCash = UMoCash;
    }
    public Date getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public String getCreateBy() {
        return this.createBy;
    }
    
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }
    public Integer getDocEntrySo() {
        return this.docEntrySo;
    }
    
    public void setDocEntrySo(Integer docEntrySo) {
        this.docEntrySo = docEntrySo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUPay() {
        return UPay;
    }

    public void setUPay(Long UPay) {
        this.UPay = UPay;
    }

    public Long getUDebt() {
        return UDebt;
    }

    public void setUDebt(Long UDebt) {
        this.UDebt = UDebt;
    }

    


}


