/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usercontrol;

import java.awt.Dialog;
import javax.swing.JDialog;
import javax.swing.JFrame;

/**
 *
 * @author TNam
 */
public class CustomDialog {

    private static JDialog jr;

    public static void ShowDialog(JFrame frm, String title)
    {
        jr = new JDialog();
        jr.setResizable(false);
        jr.setSize(frm.getSize());
        jr.setTitle(title);
        jr.setContentPane(frm.getContentPane());
        jr.setLocationRelativeTo(frm);
        jr.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        jr.setVisible(true); 
    }
    
    public static void HideDialog(JDialog jr)
    {
        jr.dispose();
    }
    
    public static void CloseDialog()
    {
        if(jr != null){
            jr.dispose();
        }
    }
}
