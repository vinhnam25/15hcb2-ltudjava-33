/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appbanhang33_beta;

import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import pojo.Customers;
import pojo.Distributors;
import serv.Customers_DAL;
import serv.Distributors_DAL;
import serv.Regions_DAL;
import usercontrol.CustomDialog;

/**
 *
 * @author NgocLam
 */
public class JF_NhaPhanPhoi extends javax.swing.JFrame {

    /**
     * Creates new form JF_NhaPhanPhoi
     */
    public JF_NhaPhanPhoi() {
        initComponents();
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(0).setHeaderValue("Mã");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(1).setHeaderValue("Tên");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(2).setHeaderValue("Khu vực");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(3).setHeaderValue("Địa chỉ");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(4).setHeaderValue("Mã số thuế");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(5).setHeaderValue("Fax");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(6).setHeaderValue("Tình trạng");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(7).setHeaderValue("Điện thoại");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(8).setHeaderValue("Mobile");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(9).setHeaderValue("Website");  
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(10).setHeaderValue("Email");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(11).setHeaderValue("Tài khoản");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(12).setHeaderValue("Ngân hàng");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(13).setHeaderValue("Giới hạn nợ");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(14).setHeaderValue("Nợ hiện tại");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(15).setHeaderValue("Chiết khấu");
        this.jTableNhaPhanPhoi.getColumnModel().getColumn(16).setHeaderValue("Chức vụ");
        
        model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
               //all cells false
               return false;
            }
        };
        jTableNhaPhanPhoi.setModel(model);
        model.addColumn("Mã");
        model.addColumn("Tên");
        model.addColumn("Khu vực");
        model.addColumn("Địa chỉ");
        model.addColumn("Mã số thuế");
        model.addColumn("Fax");
        model.addColumn("Tình trạng");
        model.addColumn("Điện thoại");
        model.addColumn("Mobile");
        model.addColumn("Website");
        model.addColumn("Email");
        model.addColumn("Tài khoản");
        model.addColumn("Ngân hàng");
        model.addColumn("Giới hạn nợ");
        model.addColumn("Nợ hiện tại");
        model.addColumn("Chiết khấu");
        model.addColumn("Chức vụ");
        Load();
    }
    
    private void Load(){
        model.setRowCount(0);
        List<Distributors> list = Distributors_DAL.GetAllDistributors();
        for(Distributors distributor : list)
        {
            Vector v = new Vector();
            v.add(distributor.getDistCode());
            v.add(distributor.getDistName());
            v.add(Regions_DAL.GetRegionNameById(distributor.getRegion()));
            v.add(distributor.getAddress());
            v.add(distributor.getTaxcode());
            v.add(distributor.getFax());
            v.add(distributor.getStatus().equals("A")?"Còn quản lý":"Không còn quản lý");
            v.add(distributor.getPhone());
            v.add(distributor.getMobile());
            v.add(distributor.getWebsite());
            v.add(distributor.getEmail());
            v.add(distributor.getTaikhoan());
            v.add(distributor.getBank());
            v.add(distributor.getLimitOwe());
            v.add(distributor.getCurrOwe());
            v.add(distributor.getDiscount());
            v.add(distributor.getJobtitle());
            model.addRow(v);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTableNhaPhanPhoi = new javax.swing.JTable();
        btnThem = new javax.swing.JButton();
        btnXoa = new javax.swing.JButton();
        btnSua = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTableNhaPhanPhoi.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Ma", "Ten", "Khu vuc", "Dia chi", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9", "Title 10", "Title 11", "Title 12", "Title 13", "Title 14", "Title 15", "Title 16", "Title 17"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTableNhaPhanPhoi);

        btnThem.setText("Thêm");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        btnXoa.setText("Xóa");
        btnXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaActionPerformed(evt);
            }
        });

        btnSua.setText("Sửa");
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(259, 621, Short.MAX_VALUE)
                .addComponent(btnThem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnXoa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSua)
                .addContainerGap())
            .addComponent(jScrollPane2)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSua)
                    .addComponent(btnXoa)
                    .addComponent(btnThem))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        // TODO add your handling code here:
        JF_CapNhatNhaPhanPhoi frm = new JF_CapNhatNhaPhanPhoi();
        frm.setTitle("Thêm nhà phân phối");
        CustomDialog.ShowDialog(frm, frm.getTitle());
        Load(); 
    }//GEN-LAST:event_btnThemActionPerformed

    private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaActionPerformed
        // TODO add your handling code here:
        if(jTableNhaPhanPhoi.getSelectedRow() != -1){
            Distributors distributor = new Distributors(
                    (Integer)jTableNhaPhanPhoi.getValueAt(jTableNhaPhanPhoi.getSelectedRow(), 0));
            if(Distributors_DAL.DeleteDistributor(distributor)){
                JOptionPane.showMessageDialog(null, "Xóa nhà phân phối thành công.", "Thông báo", 1);
                Load();
            }else{
                JOptionPane.showMessageDialog(null, "Không thể xóa nhà phân phối. Vui lòng kiểm tra lại.", "Lỗi", 1);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Vui lòng chọn nhà phân phối muốn xóa.", "Lỗi", 1);
        }
    }//GEN-LAST:event_btnXoaActionPerformed

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        // TODO add your handling code here:
        if(jTableNhaPhanPhoi.getSelectedRow() != -1){
            Distributors distributor = new Distributors(
                    (Integer)jTableNhaPhanPhoi.getValueAt(jTableNhaPhanPhoi.getSelectedRow(), 0));
            JF_CapNhatNhaPhanPhoi frm = new JF_CapNhatNhaPhanPhoi(distributor);
            frm.setTitle("Sửa nhà phân phối");
            CustomDialog.ShowDialog(frm, frm.getTitle());
            Load();
        }else{
            JOptionPane.showMessageDialog(null, "Vui lòng chọn nhà phân phối muốn sửa.", "Lỗi", 1);
        }
    }//GEN-LAST:event_btnSuaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JF_NhaPhanPhoi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JF_NhaPhanPhoi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JF_NhaPhanPhoi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JF_NhaPhanPhoi.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JF_NhaPhanPhoi().setVisible(true);
            }
        });
    }
    
    DefaultTableModel model;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnXoa;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableNhaPhanPhoi;
    // End of variables declaration//GEN-END:variables
}
